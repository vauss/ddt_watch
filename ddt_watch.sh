#!/bin/bash

# on efface l'écran ;)
clear

# on vérifie la présence du fichier Translation-fr.bz2 en local
test -f Translation-fr.bz2
if [[ $(echo $?) == 1 ]]
 then 
 echo "Le fichier Translation-fr.bz2 n'existe pas en local, arrêt."
 exit
fi

# on récupère la dernière version du fichier Translation-fr.bz2
echo "Récupération de la dernière version de Translation-fr.bz2..."
wget http://ftp.debian.org/debian/dists/sid/main/i18n/Translation-fr.bz2 -qO Translation-fr.bz2.new

# on compare les sommes sha256 ; si fichiers identiques on stoppe, sinon on continue.
if [[ $(sha256sum Translation-fr.bz2 | cut -d " " -f1) == $(sha256sum Translation-fr.bz2.new | cut -d " " -f1) ]]
 then
 echo "Fichiers Translation-fr identiques, arrêt."
 rm Translation-fr.bz2.new
 exit
fi
echo "Fichiers Translation-fr différents, on continue !"

# on récupère la liste des noms de paquets dont les descriptions traduites sont sorties
echo "Listage des paquets supprimés des traductions (listés dans diff.txt)..."
bunzip2 -k Translation-fr.bz2
bunzip2 -k Translation-fr.bz2.new -c > Translation-fr.new
diff -u Translation-fr Translation-fr.new | grep -e "-Package:" | sed 's/-Package: //' > diff.txt

# on enlève de la liste celles sorties car leur paquet a été enlevé des dépôts
echo "Vérification que les paquets existent toujours dans le dépôt Sid..."
wget http://ftp.debian.org/debian/dists/sid/main/i18n/Translation-en.bz2 -qO Translation-en.bz2
bunzip2 Translation-en.bz2
echo "--$(date)--" >> log.txt
echo "("$(cat Translation-fr.new |grep -c "Package:")" descriptions traduites sur "$(cat Translation-en |grep -c "Package:")")" | tee -a log.txt
echo "Paquets à re-traduire dans le DDTSS : " | tee -a log.txt
trouve=0
for i in $(cat diff.txt)
do
 cat Translation-en | grep -qx "Package: $i"
 if [[ $? == 0 ]]
  then
  echo "  * $i" | tee -a log.txt
  trouve=$(( $trouve + 1 )) 
 fi
done
if [[ $trouve == 0 ]]
 then
 echo '  [aucun paquet]' | tee -a log.txt
fi
echo -e "--"$(date | tr -c "-" "-")"-\n" >> log.txt
echo "Changements écrits dans log.txt"

# on réactualise le fichier Translation-fr.bz2 avec la nouvelle version
echo "Fichier Translation-fr.bz2 mis à jour à sa dernière version"
mv -f Translation-fr.bz2.new Translation-fr.bz2
rm Translation-fr Translation-fr.new Translation-en