Il s'agit d'un outil pour l'équipe de traduction française de
Debian travaillant sur le DDTP, le projet de traduction des 
descriptions des paquets Debian.
Ce script récupère les noms des paquets dont les descriptions 
anglaises ont changé, « brisant » ainsi la traduction française.

Pour que ce script fonctionne, il est nécessaire d'avoir dans le
même répertoire que le script une copie du fichier 
Translation-fr.bz2 de Sid à une version précédente (par exemple,
celle de la veille). 
Ce script est prévu pour être lancé quotidiennement (dans la
mesure où les fichiers Translation-* de Sid sont recréés chaque
jour).


Licence : GPL version 2